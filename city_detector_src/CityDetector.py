#!/usr/bin/python

# Standard imports
import cv2
import numpy as np;


NAME = 'nasz detect'
TYPE = 'CVRP'
COMMENT = ''
EDGE_WEIGHT_TYPE = 'EUC_2D'
CAPACITY = '1000'

# Read image
im = cv2.imread("miasta2.jpg", cv2.IMREAD_GRAYSCALE)
ret,im = cv2.threshold(im,127,255,cv2.THRESH_BINARY)
im = cv2.blur(im,(10,10))
im = cv2.resize(im, (0,0), fx=0.4, fy=0.4)
# Setup SimpleBlobDetector parameters.
params = cv2.SimpleBlobDetector_Params()

# # Change thresholds
# params.minThreshold = 10
# params.maxThreshold = 200
#
# # Filter by Area.
# params.filterByArea = True
# params.minArea = 1500
#
# # Filter by Circularity
# params.filterByCircularity = True
# params.minCircularity = 0.1
#
# # Filter by Convexity
# params.filterByConvexity = True
# params.minConvexity = 0.87
#
# # Filter by Inertia
# params.filterByInertia = True
# params.minInertiaRatio = 0.01

# Create a detector with the parameters
ver = (cv2.__version__).split('.')
if int(ver[0]) < 3:
    detector = cv2.SimpleBlobDetector(params)
else:
    detector = cv2.SimpleBlobDetector_create(params)

# Detect blobs.
keypoints = detector.detect(im)

DIMENSION = str(len(keypoints))

f = open('myfile.vrp', 'w')
f.write('NAME : ' + NAME + '\nCOMMENT : ' + COMMENT + '\nTYPE : ' + TYPE + '\nDIMENSION : ' + DIMENSION + '\nEDGE_WEIGHT_TYPE : ' + EDGE_WEIGHT_TYPE + '\nCAPACITY : ' + CAPACITY + '\nNODE_COORD_SECTION \n')

for i, keypoint in enumerate(keypoints):
    x, y = keypoint.pt
    f.write(" " + str(i+1) + " " + str(int(x/10)) + " " + str(int(y/10))+ "\n")

f.write('DEMAND_SECTION \n')

for i, keypoint in enumerate(keypoints):
    size = keypoint.size
    if size > 100:
        size = 100
    f.write(str(i+1) + ' ' + str(int(size)) + "\n")

f.write('DEPOT_SECTION\n 1\n -1\nEOF')

f.close()  # you can omit in most cases as the destructor will call it

# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures
# the size of the circle corresponds to the size of blob

im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255),
                                      cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

# Show blobs
cv2.imshow("Keypoints", im_with_keypoints)
cv2.waitKey(0)
