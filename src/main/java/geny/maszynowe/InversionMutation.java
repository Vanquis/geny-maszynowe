package geny.maszynowe;

import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by quirell on 23.11.2016.
 */
public class InversionMutation implements EvolutionaryOperator<List<Coordinate>> {

    private NumberGenerator<Integer> range;
    private double probability;

    public InversionMutation(NumberGenerator<Integer> range, double probability) {
        this.range = range;
        this.probability = 1 - probability;
    }

    @Override
    public List<List<Coordinate>> apply(List<List<Coordinate>> list, Random random) {
        ArrayList<List<Coordinate>> result = new ArrayList<>(list.size());
        for (List<Coordinate> route : list) {
            if (random.nextDouble() < probability)
                continue;
            int start = random.nextInt(route.size());
            int end = start + range.nextValue();
            if (end > route.size()) {
                start -= end - route.size() - 1;
                end = route.size();
            }
            ArrayList<Coordinate> r = new ArrayList<>(route.size());
            for (int i = 0; i < start; i++) {
                r.add(route.get(i));
            }
            for (int i = end - 1; i >= start; i--) {
                r.add(route.get(i));
            }
            for (int i = end; i < route.size(); i++) {
                r.add(route.get(i));
            }
            result.add(r);

        }
        return result;
    }
}
