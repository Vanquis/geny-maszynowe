package geny.maszynowe;

import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class Runner {

    public static void main(String args[]) throws IOException, ParseException {
        String testFile = args[0];
        List<String> results = new LinkedList<>();
        for (int popl = 30, evol = 5; popl < 90; popl += 30, evol += 5) {
            for (double orpr = 0.5; orpr < 1.0; orpr += 0.05) {
                for (int range = 1; range < 4; range++) {
                    for (int mutnum = 1; mutnum < 4; mutnum++) {
                        double avg = 0;
                        int total = 10;
                        for (int i = 0; i < total; i++) {
//                            avg += CVRP.main(createArgs(testFile,orpr,range,mutnum,popl,evol));
                        }
                        results.add(printResult(orpr, range, mutnum, popl, evol, avg / total));
                    }
                }
            }
        }
        System.out.println("Following lines contain results and params for file: " + testFile);
        System.out.println("result probability mutrange mutnum poplsize elitsize");
        for (String result : results) {
            System.out.println(result);
        }

    }

    private static String[] createArgs(String file, double orpr, int range, int mutnum,int popl,int evol) {
        return String.format(Locale.US,"-cral -popls %d -elit %d -orpr %f -maxr %d -minr 1 -minn 1 -maxn %d -test %s -iters 100000 -stag 10000",popl,evol,orpr,range,mutnum,file).split(
                " ");
    }

    private static String printResult(double orpr, int range, int mutnum,int popl,int evol,double avg) {
        return String.format(Locale.US, "%f %f %d %d %d %d",avg,orpr,range,mutnum,popl,evol);
    }
}
