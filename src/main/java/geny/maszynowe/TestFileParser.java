package geny.maszynowe;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by quirell on 18.11.2016.
 */
public class TestFileParser {

    private List<String> readLines(String testPath) throws IOException {
        Charset charset = Charset.forName("ASCII");
        List<String> lines = Files.readAllLines(Paths.get(testPath), charset);
        return lines;
    }

    public TestFile readTestFile(String testPath) throws IOException {
        ArrayList<Coordinate> coords = new ArrayList<>();
        List<String> lines = readLines(testPath);
        int dimension = Integer.parseInt(lines.get(Position.Dimension).split(":")[1].trim());
        int capacity = Integer.parseInt(lines.get(Position.Capacity).split(":")[1].trim());
        for (int i = Position.FirstCoord,id = 0; i < dimension+Position.FirstCoord; i++,id++) {
            int x = Integer.parseInt(lines.get(i).split(" ")[2].trim());
            int y = Integer.parseInt(lines.get(i).split(" ")[3].trim());
            int demand = Integer.parseInt(lines.get(i+dimension+Position.SectionBreak).split(" ")[1].trim());
            coords.add(new Coordinate(id,x,y,demand));
        }
        TestFile testFile = new TestFile(capacity,coords);
        return testFile;
    }

    private class Position{
        private static final int Dimension = 3;
        private static final int Capacity = 5;
        private static final int FirstCoord = 7;
        private static final int SectionBreak = 1;
    }
}
