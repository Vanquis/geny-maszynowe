package geny.maszynowe;

import org.jgrapht.Graphs;
import org.jgrapht.graph.SimpleGraph;
import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by quirell on 25.11.2016.
 */
public class GraphCrossover extends AbstractCrossover<List<Coordinate>> {


    private final TestFile file;
    private final DistanceLookup lookup;
    private final ArrayList<Node> coords = new ArrayList<>();
    private final ArrayList<Node> positions = new ArrayList<>();

    public GraphCrossover(int crossoverPoints, TestFile file, DistanceLookup lookup) {
        super(crossoverPoints);
        this.file = file;
        this.lookup = lookup;
        List<Coordinate> customerCoords = file.getCustomerCoords();
        for (int i = 0, customerCoordsSize = customerCoords.size(); i < customerCoordsSize; i++) {
            coords.add(new Node(customerCoords.get(i), null));
            positions.add(new Node(null, i));
        }


    }

    @Override
    protected List<List<Coordinate>> mate(List<Coordinate> r1, List<Coordinate> r2, int c, Random random) {
        SimpleGraph<Node, Object> graph = new SimpleGraph<>(Object.class);
        Graphs.addAllVertices(graph, coords);
        Graphs.addAllVertices(graph, positions);
        addEdges(graph, r1);
        addEdges(graph, r2);

        return null;
    }

    private void visit(Node node, SimpleGraph<Node, Object> graph) {

    }

    private void addEdges(SimpleGraph<Node, Object> graph, List<Coordinate> route) {
        for (int i = 0; i < route.size(); i++) {
            //id-1 because depot id is 0 and route does not contain depot
            graph.addEdge(positions.get(i), coords.get(route.get(i).getId() - 1));
        }
    }

    private class Node {
        private Coordinate coord;
        private Integer id;
        private boolean processed;

        public Node(Coordinate coord, Integer id) {
            this.coord = coord;
            this.id = id;
        }

        public Coordinate getCoord() {
            return coord;
        }

        public Integer getId() {
            return id;
        }

        public boolean isProcessed() {
            return processed;
        }

        public void setProcessed(boolean processed) {
            this.processed = processed;
        }
    }

}
