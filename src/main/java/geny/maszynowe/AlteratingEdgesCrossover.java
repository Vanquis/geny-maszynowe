package geny.maszynowe;

import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by quirell on 29.11.2016.
 */
public class AlteratingEdgesCrossover extends AbstractCrossover<List<Coordinate>> {

    private final TestFile testFile;

    protected AlteratingEdgesCrossover(double probability, TestFile testFile) {
        super(2, new Probability(probability));
        this.testFile = testFile;
    }

    @Override
    protected List<List<Coordinate>> mate(List<Coordinate> r1, List<Coordinate> r2, int notUsed, Random random) {
        int[] inv1 = new int[r1.size() + 1];
        int[] inv2 = new int[r1.size() + 1];
        for (int i = 0; i < r1.size(); i++) {
            inv1[r1.get(i).getId()] = i;
            inv2[r2.get(i).getId()] = i;
        }
        return newArrayList(generateOffspring(r1, inv1, r2, inv2), generateOffspring(r2, inv2, r1, inv1));
    }

    private List<Coordinate> generateOffspring(List<Coordinate> first, int[] firstInv, List<Coordinate> second, int[] secondInv) {
        ArrayList<Coordinate> offspring = new ArrayList<>(first.size());
        boolean[] usedInOffspring = new boolean[first.size() + 1];
        add(first.get(0), offspring, usedInOffspring);
        add(first.get(1), offspring, usedInOffspring);
        for (int i = 2; i < first.size(); i++) {
            if (i % 2 == 0) {
                addNextCoord(second, secondInv, offspring, usedInOffspring);
            } else {
                addNextCoord(first, firstInv, offspring, usedInOffspring);
            }
        }
        return offspring;
    }


    private void addNextCoord(List<Coordinate> rOther, int[] invOther, ArrayList<Coordinate> offspring, boolean[] alreadyUsed) {
        int indexAtOtherParent = invOther[offspring.get(offspring.size() - 1).getId()];
        Coordinate nextCoord;
        if (endOfRoute(rOther, indexAtOtherParent)) {
            nextCoord = notUsed(alreadyUsed);
        } else {
            nextCoord = rOther.get(indexAtOtherParent + 1);
            if (alreadyUsed[nextCoord.getId()])
                nextCoord = notUsed(alreadyUsed);
        }
        add(nextCoord, offspring, alreadyUsed);
    }

    private boolean endOfRoute(List<Coordinate> rOther, int indexAtOtherParent) {
        return indexAtOtherParent + 1 == rOther.size();
    }

    private Coordinate notUsed(boolean[] used) {
        for (int i = 1; i < used.length; i++) {
            if (!used[i])
                return testFile.getCustomerCoordAt(i - 1);
        }
        throw new IllegalStateException();
    }

    private void add(Coordinate toAdd, ArrayList<Coordinate> offspring, boolean[] used) {
        offspring.add(toAdd);
        used[toAdd.getId()] = true;
    }

}
