package geny.maszynowe;
import java.util.ArrayList;

public class DistanceLookup {

    private double[][] distances;

    public DistanceLookup(TestFile testFile) {
        ArrayList<Coordinate> coordinates = new ArrayList(testFile.getCustomerCoords());
        coordinates.add(testFile.getDepotCoord());
        distances = new double[coordinates.size()][coordinates.size()];
        coordinates.forEach(c1 -> coordinates.forEach(c2 -> distances[c1.getId()][c2.getId()] = euclidean(c1,c2)));
    }

    private double euclidean(Coordinate c1, Coordinate c2) {
        return Math.sqrt((c1.getX() - c2.getX()) * (c1.getX() - c2.getX()) + (c1.getY() - c2.getY()) * (c1.getY() - c2.getY()));
    }

    public double distance(Coordinate c1, Coordinate c2) {
        return distances[c1.getId()][c2.getId()];
    }
}
