package geny.maszynowe;

import org.apache.commons.cli.ParseException;
import org.uncommons.maths.number.ConstantGenerator;
import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.DiscreteUniformGenerator;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.GenerationalEvolutionEngine;
import org.uncommons.watchmaker.framework.factories.ListPermutationFactory;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.operators.ListOrderCrossover;
import org.uncommons.watchmaker.framework.operators.ListOrderMutation;
import org.uncommons.watchmaker.framework.selection.TournamentSelection;
import org.uncommons.watchmaker.framework.termination.GenerationCount;
import org.uncommons.watchmaker.framework.termination.Stagnation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CVRP {

    private static Logger logger = Logger.getLogger("CVRP");

    public static void main(String[] args) throws ParseException, IOException {
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            logger.log(Level.INFO, throwable.getMessage(), throwable);
        });
        ProgramOptions opts = new ProgramOptions(args);

        TestFileParser testParser = new TestFileParser();
        TestFile testFile = testParser.readTestFile(opts.getTestPath());
        MersenneTwisterRNG rng = new MersenneTwisterRNG();


        List<EvolutionaryOperator<List<Coordinate>>> operators = new ArrayList<>(4);
        if (opts.isUseOrderedCrossover())
            operators.add(new ListOrderCrossover<>(new Probability(opts.getOrderedCrossoverProbability())));
        if (opts.isUseAlteratingCrossover())
            operators.add(new AlteratingEdgesCrossover(opts.getOrderedCrossoverProbability(), testFile));
        operators.add(new ListOrderMutation<>(getNumberGenerator(opts.getMinNumberOfMutations(), opts.getMaxNumberOfMutations(), rng), getNumberGenerator(opts.getMinMutationRange(), opts.getMaxMutationRange(), rng)));
        //operators.add(new InversionMutation(getNumberGenerator(opts.getMinNumberOfMutations(), opts.getMaxNumberOfMutations(), rng),opts.getInversionProbability())); not working properly ?


        SumRouteEvaluator fitnessEvaluator = new SumRouteEvaluator(new DistanceLookup(testFile), testFile, opts.getOverloadPenalty());
        EvolutionPipeline<List<Coordinate>> pipeline = new EvolutionPipeline<>(operators);
        GenerationalEvolutionEngine<List<Coordinate>> engine = new GenerationalEvolutionEngine<>(new ListPermutationFactory<>(testFile.getCustomerCoords()), pipeline, fitnessEvaluator, new TournamentSelection(new Probability(0.75)), rng);


        final int printEvery = (Math.log10(opts.getGenerations())) - 2 < 0 ? 100 : (int) Math.pow(10, Math.log10(opts.getGenerations()) - 2);
        engine.addEvolutionObserver(population -> {
            if (population.getGenerationNumber() % printEvery == 0)
                logger.info("generation " + population.getGenerationNumber() + " / " + opts.getGenerations() + " fitness: " + population.getBestCandidateFitness());

        });


        List<Coordinate> result = engine.evolve(opts.getPopulationSize(), opts.getEliteSize(), new Stagnation(opts.getStagnation(), false), new GenerationCount(opts.getGenerations()));
        logger.info("fitness : " + fitnessEvaluator.printRoute(result));
        //return fitnessEvaluator.getFitness(result,null);

    }

    private static NumberGenerator<Integer> getNumberGenerator(int min, int max, MersenneTwisterRNG rng) {
        if (min == max) {
            return new ConstantGenerator<>(max);
        } else {
            return new DiscreteUniformGenerator(min, max, rng);
        }
    }

}
