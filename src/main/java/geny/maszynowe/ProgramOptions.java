package geny.maszynowe;
import org.apache.commons.cli.*;

import java.util.function.Consumer;

/**
 * Created by quirell on 18.11.2016.
 */
public class ProgramOptions {

    private int poplSize = 30;
    private int elite = 3;
    private int generations = 10000;
    private int stagnation = Integer.MAX_VALUE;
    private String testPath;
    private final CommandLine line;
    private double orderedCrossoverProbability = 0.75;
    private double alteratingCrossoverProbability = 0.75;
    private int crossoverPoints = 1;
    private boolean useOrderedCrossover;
    private boolean useAlteratingCrossover;
    private int overloadPenalty = 2;
    private int minNumberOfMutations = 1;
    private int maxNumberOfMutations = 1;
    private int minMutationRange = 1;
    private int maxMutationRange = 3;
    private double inversionProbability = 0.1;

    public ProgramOptions(String[] args) throws ParseException {

        Options options = new Options();
        options.addOption("test", true, "test file path");
        options.addOption("help", "help");
        options.addOption("popls", true, "size of the population");
        options.addOption("iters", true, "max number of iterations");
        options.addOption("elit", true, "number of parents devolved to next generation");
        options.addOption("minn", true, "Min Number of mutations");
        options.addOption("maxn", true, "Max Number of mutations");
        options.addOption("minr", true, "Min Mutation range");
        options.addOption("maxr", true, "Max Mutation range");
        options.addOption("cror", "Whether to use ordered crossover");
        options.addOption("orpr", true, "probability of ordered crossover");
        options.addOption("cral", "Whether to use alternating crossover");
        options.addOption("alpr", true, "probability of alternating crossover");
        options.addOption("ovpe", true, "Penalty for exceeding capacity of truck (multiplier)");
        options.addOption("stag", true, "Value after which terminate algorithm if there was no improvement");
        options.addOption("inpr", true, "Probability of route inversion");

        CommandLineParser parser = new DefaultParser();
        line = parser.parse(options, args);
        if (line.hasOption("help")) {
            new HelpFormatter().printHelp("cvrp", options);
        }
        setProperty("popls", this::setPoplSize);
        setProperty("iters", this::setGenerations);
        setProperty("elit", this::setElite);
        setProperty("test", this::setTestPath);
        setProperty("crpo", this::setCrossoverPoints);
        setProperty("orpr", this::setOrderedCrossoverProbability);
        setProperty("ovpe", this::setOverloadPenalty);
        setProperty("minn", this::setMinNumberOfMutations);
        setProperty("maxn", this::setMaxNumberOfMutations);
        setProperty("minr", this::setMinMutationRange);
        setProperty("maxr", this::setMaxMutationRange);
        setProperty("alpr", this::setAlteratingCrossoverProbability);
        setProperty("stag", this::setStagnation);
        setProperty("inpr", this::setInversionProbability);
        useOrderedCrossover = line.hasOption("cror");
        useAlteratingCrossover = line.hasOption("cral");

    }

    private void setProperty(String name, Consumer<String> setter) {
        if (line.getOptionValue(name) != null)
            setter.accept(line.getOptionValue(name));
    }


    private void setPoplSize(String poplSize) {
        this.poplSize = Integer.parseInt(poplSize);
    }

    private void setElite(String elite) {
        this.elite = Integer.parseInt(elite);
    }

    private void setGenerations(String generations) {
        this.generations = Integer.parseInt(generations);
    }

    public int getPopulationSize() {
        return poplSize;
    }

    public int getEliteSize() {
        return elite;
    }

    public int getGenerations() {
        return generations;
    }

    public String getTestPath() {
        return testPath;
    }

    private void setTestPath(String testPath) {
        this.testPath = testPath;
    }

    public double getOrderedCrossoverProbability() {
        return orderedCrossoverProbability;
    }

    private void setOrderedCrossoverProbability(String orderedCrossoverProbability) {
        this.orderedCrossoverProbability = Double.parseDouble(orderedCrossoverProbability);
    }

    public int getCrossoverPoints() {
        return crossoverPoints;
    }

    private void setCrossoverPoints(String crossoverPoints) {
        this.crossoverPoints = Integer.parseInt(crossoverPoints);
    }

    public boolean isUseOrderedCrossover() {
        return useOrderedCrossover;
    }

    public int getOverloadPenalty() {
        return overloadPenalty;
    }

    public void setOverloadPenalty(String overloadPenalty) {
        this.overloadPenalty = Integer.parseInt(overloadPenalty);
    }

    public int getMinNumberOfMutations() {
        return minNumberOfMutations;
    }

    public void setMinNumberOfMutations(String minNumberOfMutations) {
        this.minNumberOfMutations = Integer.parseInt(minNumberOfMutations);
    }

    public int getMaxNumberOfMutations() {
        return maxNumberOfMutations;
    }

    public void setMaxNumberOfMutations(String maxNumberOfMutations) {
        this.maxNumberOfMutations = Integer.parseInt(maxNumberOfMutations);
    }

    public int getMinMutationRange() {
        return minMutationRange;
    }

    public void setMinMutationRange(String minMutationRange) {
        this.minMutationRange = Integer.parseInt(minMutationRange);
    }


    public int getMaxMutationRange() {
        return maxMutationRange;
    }

    public void setMaxMutationRange(String maxMutationRange) {
        this.maxMutationRange = Integer.parseInt(maxMutationRange);
    }

    public boolean isUseAlteratingCrossover() {
        return useAlteratingCrossover;
    }

    public void setUseAlteratingCrossover(boolean useAlteratingCrossover) {
        this.useAlteratingCrossover = useAlteratingCrossover;
    }

    public double getAlteratingCrossoverProbability() {
        return alteratingCrossoverProbability;
    }

    public void setAlteratingCrossoverProbability(String alteratingCrossoverProbability) {
        this.alteratingCrossoverProbability = Double.parseDouble(alteratingCrossoverProbability);
    }

    public int getStagnation() {
        return stagnation;
    }

    public void setStagnation(String stagnation) {
        this.stagnation = Integer.parseInt(stagnation);
    }

    public double getInversionProbability() {
        return inversionProbability;
    }

    public void setInversionProbability(String inversionProbability) {
        this.inversionProbability = Double.parseDouble(inversionProbability);
    }
}
