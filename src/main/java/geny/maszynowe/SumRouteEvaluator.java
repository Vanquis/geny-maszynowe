package geny.maszynowe;

import org.uncommons.watchmaker.framework.FitnessEvaluator;

import java.util.List;

public class SumRouteEvaluator implements FitnessEvaluator<List<Coordinate>> {

    private DistanceLookup distances;
    private TestFile testFile;
    private int overloadPenalty;

    public SumRouteEvaluator(DistanceLookup distances, TestFile testFile, int overloadPenalty) {
        this.distances = distances;
        this.testFile = testFile;
        this.overloadPenalty = overloadPenalty;
    }

    //fitness function includes distances between nodes and virtual depots
    @Override
    public double getFitness(List<Coordinate> route, List<? extends List<Coordinate>> list) {
        Coordinate depot = testFile.getDepotCoord();
        //account for starting and ending point not included into route
        double totalDist = distances.distance(depot, route.get(0)) + distances.distance(route.get(route.size() - 1), depot);
        int load = 0;
        double partialDist = 0;
        Coordinate prev = route.get(0);
        for (int i = 0; i < route.size(); i++) {
            Coordinate coord = route.get(i);
            load += coord.getDemand();
            if (load > testFile.getCapacity()) {
                totalDist += partialDist + distances.distance(prev, depot);
                load = coord.getDemand();
                partialDist = 0;
            }
            partialDist += distances.distance(prev, coord);
            prev = coord;
        }
        totalDist += partialDist;
        if (load > testFile.getCapacity()) {
            totalDist *= overloadPenalty;
        }
        return totalDist;
    }

    public String printRoute(List<Coordinate> route) {
        Coordinate depot = testFile.getDepotCoord();
        //account for starting and ending point not included into route
        double totalDist = distances.distance(depot, route.get(0)) + distances.distance(route.get(route.size() - 1), depot);
        int load = 0;
        double partialDist = 0;
        Coordinate prev = route.get(0);
        String routeStr = id(depot.getId());
        for (int i = 0; i < route.size(); i++) {
            Coordinate coord = route.get(i);
            load += coord.getDemand();
            if (load > testFile.getCapacity()) {
                totalDist += partialDist + distances.distance(prev, depot);
                routeStr += id(depot.getId()) + " [cost: " + (load - coord.getDemand()) + " dist " + partialDist + "] ";
                load = coord.getDemand();
                partialDist = 0;
            }
            routeStr += id(coord.getId());
            partialDist += distances.distance(prev, coord);
            prev = coord;
        }
        totalDist += partialDist;
        routeStr += id(depot.getId()) + " [cost: " + load + " dist " + partialDist + "] [totalDist " + totalDist + "]";
        return routeStr;
    }

    private String id(int id) {
        return Integer.toString(id) + " ";
    }

    @Override
    public boolean isNatural() {
        return false;
    }
}
