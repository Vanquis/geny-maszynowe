package geny.maszynowe;
import java.util.List;

/**
 * Created by quirell on 18.11.2016.
 */
public class TestFile {

    private int capacity;
    private List<Coordinate> coordinates;
    private final Coordinate depot;

    public TestFile(int capacity, List<Coordinate> coordinates) {
        this.capacity = capacity;
        this.coordinates = coordinates;
        depot = coordinates.stream().filter(c -> c.getDemand() == 0).findFirst().get();
        coordinates.remove(depot);
    }

    public int getCapacity() {
        return capacity;
    }

    public Coordinate getCustomerCoordAt(int i) {
        return coordinates.get(i);
    }

    public List<Coordinate> getCustomerCoords() {
        return coordinates;
    }

    public Coordinate getDepotCoord() {
        return depot;
    }
}
